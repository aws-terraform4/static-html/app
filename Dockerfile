FROM nginx

COPY build/index.html /usr/share/nginx/html

ENTRYPOINT ["nginx", "-g", "daemon off;"]
